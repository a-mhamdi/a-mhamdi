## Hi, I'm **Abdelbacet Mhamdi**

I really enjoy programming in `Julia`, `Python`, `Matlab`, `LabVIEW`, and using `LaTeX` for typesetting. My OS of choice is `Linux`. It's a lot of fun for me to work with these tools and I feel like I learn something new every time I use them.

#### TIME TRACKING

[![wakatime](https://wakatime.com/badge/user/a7e05912-c632-43ea-8993-3a4a3d6118b3.svg)](https://wakatime.com/@a7e05912-c632-43ea-8993-3a4a3d6118b3)

##### Languages over Last Year [*(Link)*](https://wakatime.com/share/@a_mhamdi/ba7276b2-8878-40af-96f3-b56a9c6cee13.svg)

<img width="600em" src="https://wakatime.com/share/@a_mhamdi/3ceb72e6-40c9-49ee-a822-4a029e9388e5.svg">

##### Editors over Last Year [*(Link)*](https://wakatime.com/share/@a_mhamdi/b314d72c-e7b3-4aea-a32e-3bc42990a1d8.svg)

<img width="600em" src="https://wakatime.com/share/@a_mhamdi/ece479d0-5389-43bb-857f-3ad45b01a8ae.svg">

##### OS over Last Year [*(Link)*](https://wakatime.com/share/@a_mhamdi/b502e890-3ebc-42e2-a237-3296503ed8f1.svg)

<img width="600em" src="https://wakatime.com/share/@a_mhamdi/40e71958-d880-4dbd-afed-a79bf0ec5af1.svg">

[![Github](https://upload.wikimedia.org/wikipedia/commons/thumb/2/29/GitHub_logo_2013.svg/320px-GitHub_logo_2013.svg.png)](https://github.com/a-mhamdi)

[![Bitbucket](https://upload.wikimedia.org/wikipedia/commons/thumb/c/c5/Bitbucket-Logo-blue.svg/320px-Bitbucket-Logo-blue.svg.png)](https://bitbucket.org/abmhamdi)
